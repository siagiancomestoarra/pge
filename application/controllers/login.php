<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        if ($this->input->post('submit')) {

            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            $level = $this->input->post('level');

            $sql_res = $this->db->query("
                      SELECT a.* FROM t_user a
                      WHERE a.username='" . $username . "'
                      AND a.password='" . $password . "'
                      AND level='" . $level . "'");
            if ($sql_res->num_rows()) {
                $sql_sess = $sql_res->row();
                $sess = array(
                    'USERAME' => $username,
                    'LEVEL'   => $level,
                    'FUNGSI'  => $sql_sess->id_fungsi,
                    'ISLOGIN' => true
                );
                $this->session->set_userdata( $sess );
                redirect('manage');

            } else {
                $data['err'] = 'Maaf, Username atau Password Salah!!';
            }
        }

        $data['sql_level'] = $this->db->query("SELECT `level` FROM t_user GROUP BY `level`");

        $this->load->view('login', $data);

    }

    public function logout()
    {

        $sess = array(
            'USERAME' => '',
            'LEVEL'   => '',
            'ISLOGIN' => false
        );
        $this->session->unset_userdata($sess);
        $this->session->sess_destroy();
        redirect( 'login' );

    }
}
